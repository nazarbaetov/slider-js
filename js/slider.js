let showPrevBtn = document.getElementById("showPrev");
let showNextBtn = document.getElementById("showNext");
let bandSlide = document.getElementsByClassName("band")[0];
let bandSlideImages = bandSlide.querySelectorAll("img");

showPrevBtn.addEventListener("click", onShowPrevBtnClick);
showNextBtn.addEventListener("click", onShowNextBtnClick);

let currentImageIndex = 1;
let countLittleSlide = 20;
let timer;
let isClicked = false;

function onShowPrevBtnClick() {
    if (isClicked) return false;
    isClicked = true;
    currentImageIndex--;
    sliderMove("left");
}

function onShowNextBtnClick() {
    if (isClicked) return false;
    isClicked = true;
    currentImageIndex++;
    sliderMove("right");
}

function sliderMove(pos) {
    pos = pos === "left" ? -1 : 1;

    timer = setInterval(function() {
        countLittleSlide = countLittleSlide + pos;
        bandSlide.style.marginLeft = -40 * countLittleSlide + "px";

        if (countLittleSlide === (20 * currentImageIndex)) {
            clearInterval(timer);
            isLastOrFirst();
            isClicked = false;
        }
    }, 35);
}

function isLastOrFirst() {
    if (currentImageIndex <= 0) {
        countLittleSlide = 20 * (bandSlideImages.length-2);
        currentImageIndex = bandSlideImages.length-2;
        bandSlide.style.marginLeft = -40 * countLittleSlide + "px";
    }

    if (currentImageIndex >= bandSlideImages.length-1) {
        countLittleSlide = 20;
        currentImageIndex = 1;
        bandSlide.style.marginLeft = -40 * countLittleSlide + "px";
    }
}